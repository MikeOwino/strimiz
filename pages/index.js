import styles from "../styles/Home.module.css";
import Script from "next/script";
import Head from 'next/head'
// import kids from "./images/slone.jpg";
// import pic from '../public/slone.jpg'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>The Woman King</title>
        <link
          href="https://mikeowino.com/img/emojis/technologist.png"
          rel="shortcut icon"
        />
        <link
          rel="stylesheet"
          href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Questrial"
        />
        <link rel="icon" href="/favicon.png" />
        <link rel="stylesheet" href="https://cdn.plyr.io/3.6.12/plyr.css" />
      </Head>
      <video
        id="my-video"
        className="video-js"
        controls
        preload="auto"
        poster="https://static1.srcdn.com/wordpress/wp-content/uploads/2022/04/First-Titanic-movie-released-85-years-before-James-camerons-movie.jpg"
        data-setup=""
        loop
      >
        <source
          src="https://de22.seedr.cc/ff_get/1551276097/Titanic.1997.720p.HDTV.x264-YIFY.mp4?st=EIf4_fL88U1YTcqyy2AtGA&e=1687693745"
          type="video/mp4"
        />
      </video>
        <Script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/5.8.2/video.min.js"></Script>
    </div>
  );
}
